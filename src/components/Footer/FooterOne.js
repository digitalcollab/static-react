import React from "react";
import logoFooter from "../../assets/images/dc-w-2.png";
import FooterCopyright from "./FooterCopyright";

const FooterOne = () => (
  <>
    <footer className="footer" id="footer-fixed">
      <div className="footer-main">
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <div className="widget widget-text">
                <div className="logo logo-footer">
                  <a href={`${process.env.PUBLIC_URL}/`}>
                    <img
                      className="logo logo-display"
                      src={logoFooter}
                      alt=""
                    />
                  </a>
                </div>
                <p>
                  We are a fully in-house digital agency focusing on branding,
                  marketing, web design and development with clients ranging
                  from start-ups, Lorem ipsum dolor sit amet, consectetur
                  adipiscing elit. Sed varius quam ut magna ultricies
                  pellentesque.
                </p>
              </div>
            </div>
            <div className="col-sm-6 col-md-2">
              <div className="widget widget-links">
                <h5 className="widget-title">Work With Us</h5>
                <ul>
                  <li>
                    <a href="#!">Development</a>
                  </li>
                  <li>
                    <a href="#!">Design</a>
                  </li>
                  <li>
                    <a href="#!">Social Media</a>
                  </li>
                  <li>
                    <a href="#!">Social Media</a>
                  </li>
             
                  <li>
                    <a href="#!">Social Media</a>
                  </li>
             
                </ul>
              </div>
            </div>
            <div className="col-sm-6 col-md-2">
              <div className="widget widget-links">
                <h5 className="widget-title">Useful Links</h5>
                <ul>
                  <li>
                    <a href="about-us.html">About Us</a>
                  </li>
                  <li>
                    <a href="contact-simple.html">Contact Us</a>
                  </li>
                  <li>
                    <a href="service-simple.html">Our Services</a>
                  </li>
                  <li>
                    <a href="term-condition.html">Terms &amp; Conditions</a>
                  </li>
                 
                </ul>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="widget widget-text widget-links">
                <h5 className="widget-title">Contact Us</h5>
                <ul>
                  <li>
                    <i className="icofont icofont-google-map"></i>
                    <a href="#!">Howick</a>
                  </li>
                  <li>
                    <i className="icofont icofont-iphone"></i>
                    <a href="#!">09 222 2630</a>
                  </li>
                  <li>
                    <i className="icofont icofont-mail"></i>
                    <a href="#!">hello@digitalcollab.co.nz</a>
                  </li>
                  <li>
                    <i className="icofont icofont-globe"></i>
                    <a href="https://thedigitalcollab.co.nz/">www.thedigitalcollab.co.nz</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <FooterCopyright />
    </footer>
    <div className="footer-height" style={{ height: "463px" }}></div>
  </>
);

export default FooterOne;
