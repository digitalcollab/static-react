import React, { forwardRef } from "react";
import ContactForm from "./ContactForm";
import Map from "../Maps/Map";
import Iframe from "react-iframe"

const ContactUs = forwardRef((props, ref) => (
    <section className="contact-us white-bg" id="contact" ref={ref}>
      <div className="container">
        <div className="clearfix">
          <div className="bg-flex-right col-md-6 map-section">
         <Iframe url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12762.8903919531!2d174.9136785402285!3d-36.89698397885836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d4b230799ba61%3A0x500ef6143a2ce80!2sHowick%2C%20Auckland!5e0!3m2!1sen!2snz!4v1594184448320!5m2!1sen!2snz" height="100%" width="100%"/>
          </div>
          <div className="col-about-left col-md-6 text-left">
            <ContactForm title="Contact Us" tagline="Stay in Touch" />
          </div>
        </div>
      </div>
    </section>
  ));

export default ContactUs;
